
function get(){
    fetch("/api")
    .then(res => res.json())
    .then(rr=>{
        document.getElementById('host').innerHTML=rr.hostname
        document.getElementById('answer').innerHTML=rr.value
    }) 
}

function post(){
    fetch("/api",{method: 'POST'})
    .then(res => res.json())
    .then(rr=>{
        document.getElementById('host').innerHTML=rr.hostname
        document.getElementById('answer').innerHTML=""
    }) 
}