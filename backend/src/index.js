
const express = require('express'),
    bodyParser = require('body-parser'),
    os = require('os'),
    app = express(),
    port = process.env.PORT || 3000,
    cassandra_host = process.env.CASSANDRA_HOST || '127.0.0.1';
console.log(cassandra_host)
const cassandra = require('cassandra-driver');


const hostname = os.hostname() ;

app.use(bodyParser.json());
app.use(express.static('static'))
app.use(function (req, res, next) {
    res.setHeader("Connection","close")
    next()
})
  
const client = new cassandra.Client({contactPoints: [cassandra_host]});

client.connect(function(e) {
    let query = "CREATE KEYSPACE IF NOT EXISTS demo  WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1' }";
    return client.execute(query, function(e, res) {
        if (e) {
            throw e
        }
        query = "CREATE TABLE IF NOT EXISTS demo.player (key text , value int, PRIMARY KEY ((key)) )";
        return client.execute(query, function(e, res) {
            if (e) {
                throw e
            }
            return console.log(e);
        })
    });
  });
  

app.route('/api')
    .get(function(req, res) {
        let query = "SELECT value FROM demo.player WHERE key='chave'"
        client.execute(query, (e, result) => {
            let newValue=0;
            if (result.rows.length>0 ) {
                newValue = result.rows[0]['value']
            }
            
            res.send( {hostname:hostname,value:newValue});
        });        
    })
    .post(function(req, res) {
        let query = "SELECT value FROM demo.player WHERE key='chave'"
        client.execute(query, (e, result) => {
            let newValue=0
            if (result.rows.length>0 ) {
                newValue = result.rows[0]['value']
            }
            newValue=newValue+1
            query = "UPDATE demo.player SET value="+newValue+" WHERE key='chave'"
            client.execute(query, function(e, result) {
                res.send( {hostname:hostname,value:newValue});
            })
        });        
    });

app.listen(port);
console.log('API server started on: ' + port);
