
docker run -d -p 5000:5000 --restart=always --name registry registry:2

docker run -it -d -p 8080:8080 --name visualizer -v /var/run/docker.sock:/var/run/docker.sock dockersamples/visualizer


docker-compose -f docker-compose.yml  -f stack1.yml  config  > /tmp/stack1.yml 

docker-compose -f /tmp/stack1.yml up

docker stack deploy -c /tmp/stack1.yml  stack1

docker-compose -f docker-compose.yml  -f stack2.yml  config  > /tmp/stack2.yml 

docker stack deploy -c /tmp/stack2.yml  stack2
