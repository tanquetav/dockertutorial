# build with docker build -t  m .
FROM node:carbon-slim

COPY backend/package.json backend/
RUN cd backend && \
    npm i 
ADD backend backend
WORKDIR backend

CMD npm start
